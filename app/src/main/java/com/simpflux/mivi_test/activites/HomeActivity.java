package com.simpflux.mivi_test.activites;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.simpflux.mivi_test.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Created by dhanrajck on 10-10-18.
 */
public class HomeActivity extends AppCompatActivity {
    TextView txtName,txtPhone,txtEmail;
    TextView txtType,txtDOB,txteStatus,txtsStatus;
    TextView txtMsn,txtCredit,txtExpiry,txtThreshold;
    TextView txtBalance,txtExpiryDate,txtRenewal,txtPrimary;
    TextView txtpName,txtUnlimitedText,txtUnlimitedTalk,txtUnlimitediText,txtUnlimitediTalk,txtPrice;
    ImageView imgLogout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        intializeUI();
        setData();
    }

    private void intializeUI()
    {
        imgLogout=findViewById(R.id.img_logout);
        imgLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean("isLoggedIn", false);
                editor.commit();
                startActivity(new Intent(HomeActivity.this,SignInActivity.class));
            }
        });

        txtName=findViewById(R.id.txtName);
        txtPhone=findViewById(R.id.txtPhone);
        txtEmail=findViewById(R.id.txtEmail);

        txtType=findViewById(R.id.txt_payment_type);
        txtDOB=findViewById(R.id.txt_date_of_birth);
        txteStatus=findViewById(R.id.txt_email_status);
        txtsStatus=findViewById(R.id.txt_email_subscription_status);

        txtMsn=findViewById(R.id.txt_msn);
        txtCredit=findViewById(R.id.txt_credit);
        txtExpiry=findViewById(R.id.txt_credit_expiry);
        txtThreshold=findViewById(R.id.txt_threshold);

        txtBalance=findViewById(R.id.txt_dataBalance);
        txtExpiryDate=findViewById(R.id.txt_expiry_date);
        txtRenewal=findViewById(R.id.txt_auto_renewal);
        txtPrimary=findViewById(R.id.txt_primary_subscription);

        txtpName=findViewById(R.id.txt_name);
        txtUnlimitedText=findViewById(R.id.txt_unlimited_text);
        txtUnlimitedTalk=findViewById(R.id.txt_unlimited_talk);
        txtUnlimitediText=findViewById(R.id.txt_unlimited_international_text);
        txtUnlimitediTalk=findViewById(R.id.txt_unlimited_international_talk);
        txtPrice=findViewById(R.id.txt_price);
    }

    private void setData()
    {

        InputStream is = getResources().openRawResource(R.raw.collection);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            String jsonString = writer.toString();
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONObject dataJsonObject = jsonObject.getJSONObject("data");
            JSONObject attributeJsonObject = dataJsonObject.getJSONObject("attributes");
            txtType.setText(attributeJsonObject.getString("payment-type"));
            txtName.setText(attributeJsonObject.getString("title")+". "+attributeJsonObject.getString("first-name")+" "+attributeJsonObject.getString("last-name"));
            txtDOB.setText(attributeJsonObject.getString("date-of-birth"));
            txtPhone.setText(attributeJsonObject.getString("contact-number"));
            txtEmail.setText(attributeJsonObject.getString("email-address"));
            txteStatus.setText(attributeJsonObject.getString("email-address-verified"));
            txtsStatus.setText(attributeJsonObject.getString("email-subscription-status"));

            JSONArray includedJsonArray = jsonObject.getJSONArray("included");
            JSONObject servicesObject = includedJsonArray.getJSONObject(0);
            JSONObject attributeJsonObject2 = servicesObject.getJSONObject("attributes");
            txtMsn.setText(attributeJsonObject2.getString("msn"));
            txtCredit.setText(attributeJsonObject2.getString("credit"));
            txtExpiry.setText(attributeJsonObject2.getString("credit-expiry"));
            txtThreshold.setText(attributeJsonObject2.getString("data-usage-threshold"));

            JSONObject subscriptionsObject = includedJsonArray.getJSONObject(1);
            JSONObject attributeJsonObject3 = subscriptionsObject.getJSONObject("attributes");
            txtBalance.setText(attributeJsonObject3.getString("included-data-balance"));
            txtExpiryDate.setText(attributeJsonObject3.getString("expiry-date"));
            txtRenewal.setText(attributeJsonObject3.getString("auto-renewal"));
            txtPrimary.setText(attributeJsonObject3.getString("primary-subscription"));

            JSONObject productsObject = includedJsonArray.getJSONObject(2);
            JSONObject attributeJsonObject4 = productsObject.getJSONObject("attributes");
            txtpName.setText(attributeJsonObject4.getString("name"));
            txtUnlimitedText.setText(attributeJsonObject4.getString("unlimited-text"));
            txtUnlimitedTalk.setText(attributeJsonObject4.getString("unlimited-talk"));
            txtUnlimitediText.setText(attributeJsonObject4.getString("unlimited-international-text"));
            txtUnlimitediTalk.setText(attributeJsonObject4.getString("unlimited-international-talk"));
            txtPrice.setText(attributeJsonObject4.getString("price"));

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
