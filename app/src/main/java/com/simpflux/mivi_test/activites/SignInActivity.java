package com.simpflux.mivi_test.activites;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.simpflux.mivi_test.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Created by dhanrajck on 10-10-18.
 */
public class SignInActivity extends AppCompatActivity{
    ImageView logo;
    EditText txtEmail, txtPassword;
    Button btnSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        logo = findViewById(R.id.logo);
        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);
        btnSignIn = findViewById(R.id.btnSignIn);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });

    }

    private void validate() {
        InputStream is = getResources().openRawResource(R.raw.collection);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            String jsonString = writer.toString();
            JSONObject jsonObject = new JSONObject(jsonString);
            if (!txtEmail.getText().toString().equals(jsonObject.getJSONObject("data").getJSONObject("attributes").getString("email-address"))) {
                txtEmail.setError("Invalid Email");
            } else if (!txtPassword.getText().toString().equals(jsonObject.getJSONArray("included").getJSONObject(0).getJSONObject("attributes").getString("msn"))) {
                txtPassword.setError("Invalid Password");
            } else {
                gotohome();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void gotohome() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("isLoggedIn", true);
        editor.commit();
        startActivity(new Intent(SignInActivity.this, HomeActivity.class));
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

}
